<?php

class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->isLogin();

        $this->load->view('admin/dash');
         
    }

    public function create()
    {
        $this->isLogin();
        $this->load->view('admin/create');
    }

    public function accounts()
    {
        $this->isLogin();
        $data['Users'] = $this->Account_model->get();

        $this->load->view('admin/list', $data);
    }
    
    
    public function files()
    {
        $this->isLogin();
        $data['Files'] = $this->Account_model->get_files();
        
        $this->load->view('admin/files', $data);
    }
    
    
    public function file_upload()
    {
        $this->isLogin();
        $data['Users'] = $this->Account_model->get();
        
        $this->load->view('admin/file_upload', $data);
    }
    

    public function delete($id)
    {
        $this->isLogin();
        if ($this->Account_model->delete($id)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Account has been Deleted !</div>');
            redirect('admin/accounts');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Account Deletion Failed!</div>');
            redirect('admin/accounts');
        }

        // echo var_dump($data);
    }
    
    public function completed_forms()
    {
        
        $this->isLogin();
        
        $id=$_SESSION['cid'];
        
        $data['Forms'] = $this->Account_model->get_forms();
        
        $this->load->view('admin/completed',$data);
        
        
    }
    
    
    
    public function file_delete()
    {
        
        $this->isLogin();
        
         
        $this->load->helper("file");
   
        
        $id=$this->input->get("id");
        
        $path=$this->input->get("path");

        if ($this->Account_model->file_delete($id) &&  unlink($path)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">File has been Deleted !</div>');
            redirect('admin/files');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">File Deletion Failed!</div>');
           redirect('admin/files');
           
        }
        
        // echo var_dump($data);
    }
    
    
    
    public function isLogin()
    {
        
        if (!isset($_SESSION['client_name']) && $_SESSION['Type'] != 'Super User' ) {
            
        
            redirect("admin/login");
        }
        
    }

    public function register()
    {
        $this->isLogin();
        // set validation rules
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('lname', 'last Name Name', 'trim|required|alpha|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');

        $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');

        $this->form_validation->set_rules('pass', 'Password', 'trim|required|md5|min_length[4]|max_length[40]');
        // $this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');

        // validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails

            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
            $this->load->view('admin/create');
            // $this->load->view('account/register');
        } else {
            // insert the user registration details into database
            $data = array(
                'Fname' => $this->input->post('fname'),
                'Lname' => $this->input->post('lname'),
                'Email' => $this->input->post('email'),
                'Mobile' => $this->input->post('mobile'),
                'Pass' => $this->input->post('pass'),
                'Type' => $this->input->post('type')
            );

            // insert form data into database
            if ($this->Account_model->insert_users($data)) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You are Successfully Registered! Login Now !!!</div>');
                redirect('admin/accounts');
            } else {
                // echo "Failed";
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('admin/create');
            }
        }
    }

    public function login()
    {
        if (isset($_SESSION['client_name'])) {} else {
            $this->session->set_userdata("device", 'mob');

            $this->load->view('admin/signin');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();

        
                redirect("admin/login");
            
    }
    
    public function download( )
    {
        
        $this->load->helper('download');
        
        $path=$this->input->get("path");
        $name=$this->input->get("name");
        
        
        $pth    =   file_get_contents($path);
        
        force_download($name.substr($path, -5), $pth);
    }
    

    public function login_check()
    {
        // get the posted values
        $email = $this->input->post("l_email");
        $password = $this->input->post("l_pass");

        // set validations
        $this->form_validation->set_rules("l_email", "Email", "trim|required|valid_email");
        $this->form_validation->set_rules("l_pass", "Password", "trim|required");

        if ($this->form_validation->run() == FALSE) {
            // validation fails
            // $this->load->view('login');
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed !</div>');

            $this->load->view('account/signin');
        } else {
            // validation succeeds

            // check if username and password is correct
            $usr_result = $this->Account_model->user_validate($email, $password);

            if ($usr_result > 0) // active user record is present
            {
                // set the session variables
                $client = $this->Account_model->get_users($email, $password);

                $fname = $client[0]['Fname'];
                $lname = $client[0]['Lname'];
                $email = $client[0]['Email'];
                $cid = $client[0]['id'];
                $Type = $client[0]['Type'];

                $sessiondata = array(
                    'client_name' => $fname . ' ' . $lname,
                    'client_fname' => $fname,
                    'client_lname' => $lname,
                    'client_email' => $email,
                    'cid' => $cid,
                  
                    'type' => $Type 
                );
                $this->session->set_userdata($sessiondata);

                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">successfully logged in your account!</div>');

                
                if($Type =="Super User")
                    redirect('admin/index');
                else 
                    redirect('franchisee/index');
                
                    
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password! </div>');

                redirect('admin/login');
            }
        }
    }
    
    public function upload()
    {
        $times= (new DateTime())->getTimestamp();
        
        $config['upload_path']          =  'assets/uploads/';

        $config['allowed_types']        = 'pdf|doc|docx';
        
        $config['file_name']             = $this->input->post('name').'_'.$times;
        $config['max_size']             = 20000;
        
        
        $this->load->library('upload', $config);
        
        
       
        
        if ( !$this->upload->do_upload('userfile'))
        {
           
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed ! '.$this->upload->display_errors().'</div>');
            
            $this->load->view('admin/file_upload' );
        }
        else
        {
            $this->form_validation->set_rules('name', 'File Name', 'trim|required|alpha|min_length[3]|max_length[30] ');
            $this->form_validation->set_rules('details', 'File Details', 'trim|required|alpha|min_length[3]|max_length[500] ');
            
            
            $data = array('upload_data' => $this->upload->data());
           
            $fileName=$this->upload->data('file_name');
            $filePath=base_url().'assets/uploads/'.$fileName;
     
            
            
            $this->Account_model->upload_file_db([
                'Name' => $this->input->post('name'),
                'Path' => $filePath,
                'FileName' => 'assets/uploads/'.$fileName,
             
                'Details' => $this->input->post('details'),
                'UserID' => $this->input->post('uid')
    
            ]);
            
            
            redirect('admin/files');
        }
    }
    
}