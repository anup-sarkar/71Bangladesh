<?php

class Franchisee extends CI_Controller
{

        public function index()
        {
            
            $this->isLogin();
            
            $id=$_SESSION['cid'];
            
            $data['Files'] = $this->Client_model->get_files($id);
            
            $this->load->view('fr/dash',$data);

                 
        }
        
  
        public function profile()
        {
            
            $this->isLogin();
            
            $id=$_SESSION['cid'];
            
            $data['User'] = $this->Client_model->get_user($id);
            
            $data['Files'] = $this->Client_model->get_files($id);
            
            $this->load->view('fr/profile',$data);
            
            
        }
        
        public function profile_update()
        {
            
            $this->isLogin();
            
            $id=$_SESSION['cid'];
            
            $data['User'] = $this->Client_model->get_user($id);
            
            $this->load->view('fr/profile_update',$data);
            
            
        }
        
        public function completed_forms()
        {
            
            $this->isLogin();
            
            $id=$_SESSION['cid'];
            
            $data['Forms'] = $this->Client_model->get_forms($id);
            
            $this->load->view('fr/completed',$data);
            
            
        }
        
        
        
        public function work_form()
        {
            
            $this->isLogin();
            
            $id=$_SESSION['cid'];
            
           
            
            $this->load->view('fr/new_work_form' );
            
            
        }
        
        
      
        public function download( )
        {
            
            $this->load->helper('download');
            
            $path=$this->input->get("path");
            $name=$this->input->get("name");
            
            
            $pth    =   file_get_contents($path);
      
            force_download($name.substr($path, -5), $pth);
        }
        
        
        public function  delete_work($id)
        {
            
            $this->isLogin();
            
          
        
            
            if ($this->Client_model->delete_work($id)  ) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Work Form has been Deleted !</div>');
                redirect('franchisee/completed_forms');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Work Form Deletion Failed!</div>');
                redirect('franchisee/completed_forms');
                
            }
            
            // echo var_dump($data);
        }
        
        
        
        
        public function update()
        {
            $this->isLogin();
            // set validation rules
            $this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[100] ');
            
            $this->form_validation->set_rules('father', 'Father Name', 'trim|min_length[3]|max_length[100] ');
            $this->form_validation->set_rules('lname', 'last Name Name', 'trim|required|alpha|min_length[3]|max_length[100] ');
            
            
            $this->form_validation->set_rules('dob', 'Date', 'trim|min_length[3]|max_length[20] ');
            
            $this->form_validation->set_rules('addr', 'Address', 'trim|min_length[3]|max_length[100] ');
            $this->form_validation->set_rules('NID', 'NID', 'trim|min_length[3]|max_length[30] ');
            $this->form_validation->set_rules('SIS', 'SIS', 'trim|min_length[3]|max_length[30] ');
     
            
            
            $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
            
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');
            
            $this->form_validation->set_rules('pass', 'Password', 'trim|md5|min_length[6]|max_length[40]');
            // $this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
            
            // validate form input
            if ($this->form_validation->run() == FALSE) {
                // fails
                
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
               
                $id=$_SESSION['cid'];
                $data['User'] = $this->Client_model->get_user($id);
                $this->load->view('franchisee/profile_update',$data);
                // $this->load->view('account/register');
            } else {
                // insert the user registration details into database
                $pass=$this->input->post('pass');
                if($pass!=null)
                {
                $data = array(
                    'Fname' => $this->input->post('fname'),
                    'Lname' => $this->input->post('lname'),
                    'FatherName' => $this->input->post('father'),
                    'DOB' => $this->input->post('dob'),
                    'Sex' => $this->input->post('sex'),
                    'Address' => $this->input->post('addr'),
                    'NID' => $this->input->post('NID'),
                    'SIS' => $this->input->post('SIS'),
                    'Slot' => $this->input->post('slot'),
                    'IDCreationDate' => $this->input->post('cr_date'),
                   
                    'Email' => $this->input->post('email'),
                    'Mobile' => $this->input->post('mobile'),
                    'Pass' => $this->input->post('pass')
                    
                );
                }
                else
                {
                    $data = array(
                        'Fname' => $this->input->post('fname'),
                        'Lname' => $this->input->post('lname'),
                        'FatherName' => $this->input->post('father'),
                        'DOB' => $this->input->post('dob'),
                        'Sex' => $this->input->post('sex'),
                        'Address' => $this->input->post('addr'),
                        'NID' => $this->input->post('NID'),
                        'SIS' => $this->input->post('SIS'),
                        'Slot' => $this->input->post('slot'),
                        'IDCreationDate' => $this->input->post('cr_date'),
                        
                        'Email' => $this->input->post('email'),
                        'Mobile' => $this->input->post('mobile')
                    
                        
                    );
                }
                
                // insert form data into database
                if ($this->Client_model->update_profile($data,$this->input->post('id'))) {
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Your Profile Is Updated !!!</div>');
                    redirect('franchisee/profile');
                } else {
                    // echo "Failed";
                    // error
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect('franchisee/profile_update');
                }
            }
        }
        
        
        
        public function insert_work()
        {
            $this->isLogin();
            
            $id=$_SESSION['cid'];
            $this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[100] '); 
    
            $this->form_validation->set_rules('lname', 'last Name Name', 'trim|required|alpha|min_length[3]|max_length[100] ');
            $this->form_validation->set_rules('rec', 'Record No', 'trim|min_length[3]|max_length[30] ');
            $this->form_validation->set_rules('policy', 'Policy', 'trim|required|min_length[3]|max_length[20] ');
            
            $this->form_validation->set_rules('policyDate', 'PolicyDate', 'trim|required|min_length[3]|max_length[20] ');
            
            $this->form_validation->set_rules('city', 'City', 'trim|required|min_length[3]|max_length[100] ');
            $this->form_validation->set_rules('gp', 'GP', 'trim|required|min_length[3]|max_length[50] ');
            $this->form_validation->set_rules('claim', 'Claim Code', 'trim|required');
      
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');
            
            $this->form_validation->set_rules('paid', 'Paid Amount', 'trim|required');
 
            if ($this->form_validation->run() == FALSE) {
 
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
                
            
               
                $this->load->view('fr/new_work_form' );
            } else {
                // insert the user registration details into database
                
                    $data = array(
                        'UserID' => $id,
                        'Fname' => $this->input->post('fname'),
                        'Lname' => $this->input->post('lname'),
                        'RecordNo' => $this->input->post('rec'),
                        'PolicyDate' => $this->input->post('policyDate'),
                        'PolicyNo' => $this->input->post('policy'),
                        'City' => $this->input->post('city'),
                        'State' => $this->input->post('state'),
                        'MaritalStatus' => $this->input->post('marital'),
                        'GpCode' => $this->input->post('gp'),
                        'ClaimDays' => $this->input->post('claim'),
                        'Mobile' => $this->input->post('mobile'),
                        
                        'PaidAmount' => $this->input->post('paid'),
                        'NetAmount' => $this->input->post('net'),
                        
                        
                        
                    );
                
                
                // insert form data into database
                if ($this->Client_model->insert_work($data)) {
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">New Work Form Has Added Successfully  !!!</div>');
                    redirect('franchisee/completed_forms');
                } else {
                    // echo "Failed";
                    // error
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect('franchisee/work_form');
                }
            }
        }
        
        
        
        
        public function isLogin()
        {
            
            if (!isset($_SESSION['client_name']) && $_SESSION['Type'] != 'Normal User' ) {
                
                
                redirect("admin/login");
            }
            
        }

}