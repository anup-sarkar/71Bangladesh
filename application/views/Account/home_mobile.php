<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
    
   ?>


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hello Hi Parking</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url(); ?>assets/mobile/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
 <meta name="theme-color" content="#000000">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">


    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/mobile/css/new-age.min.css" rel="stylesheet">

  </head>

  <body id="page-top" style="overflow: hidden;position: fixed;">
 
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style="background-color:#21252970;-webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);display: none;">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top" style="color:#007bff"> <img src="<?php echo base_url(); ?>assets/img/logo_bright.png" style="width: 100px;height: 35px;">  </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" style="color:white">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">

          	<?php  if (isset($_SESSION['client_name'])) { ?>

 
  			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>index.php/reservations/ticket" >Ticket</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo site_url('home/profile'); ?>" ><?php echo $_SESSION['client_name']; ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger"   href="<?php echo site_url('home/logout'); ?>" >Log Out</a>
            </li>



                <?php   }else{ ?>

        					
                		  <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>index.php/reservations/ticket" >Ticket</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="<?php echo site_url('home/login'); ?>" >Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger"   href="<?php echo site_url('home/signup'); ?>" >Sign Up</a>
            </li>



                   <?php   }  ?>

              

          



          </ul>
        </div>
      </div>
    </nav> 

    <header class="masthead" style="padding-top: 100px">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-7 my-auto">
            <div class="header-content mx-auto" style="margin-bottom: 50px">
              

              <input id="pac-input" name="pac-input" type="text" placeholder="Location/Venue/Address" aria-label="Search" style="/* border-radius: 0; */background-color:  transparent;border: 1px solid #ffffff;/* padding: 6px; *//* border-radius: 20px; *//* margin-top:  5px; */color: white;margin-bottom: 20px;" class="btn btn-outline btn-xl js-scroll-trigger">
<br/>

              <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger" id="search"><i class="icon-magnifier"></i>  </a>
            </div>
          </div>
          <div class="col-lg-5 my-auto">
            <div class="device-container">
              <div class="device-mockup iphone6_plus portrait white">
                <div class="centered" style="text-align: center;">
                  <img class="img img-fluid" style="width:50%" src="<?php echo base_url(); ?>assets/images/map2.png">
                  <button class="btn btn_gradient" onclick="getLocation()" style="width: 100%" ><i class="fa fa-map-marker"></i> What's Around You ? </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

   <div id="preloader">



  <div id="status">&nbsp;
  
  </div>
  
</div>
   
 
 <div id="map" style="display: none;"></div> 


    <input type="hidden" id="lat">
<input type="hidden" id="long">
<input type="hidden" id="place">
<input type="hidden" id="address">


     <script src="<?php echo base_url(); ?>assets/mobile/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/mobile/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url(); ?>assets/mobile/js/new-age.min.js"></script>
  


<script>


$("#pac-input").focus();
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 38.532675, lng: -103.784548},
            gestureHandling: 'greedy',
              disableDefaultUI: true,
       
          zoom: 3.5
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var countries = document.getElementById('country-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,

          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No place available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
          var lat=place.geometry.location.lat();
          var long=place.geometry.location.lng();
             

           $('#lat').val(lat);
           $('#long').val(long); 


          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(16);
              var lat=place.geometry.location.lat();
              var long=place.geometry.location.lng();
            
 

              $('#lat').val(lat);
              $('#long').val(long);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

 
 
          infowindow.open(map, marker);

          console.log(place.name);
          console.log(address);

          $('#place').val(place.name);
          $('#address').val(address);

        });

        // Sets a listener on a given radio button. The radio buttons specify
        // the countries used to restrict the autocomplete search.
        function setupClickListener(id, countries) {
          
        }

        setupClickListener('changecountry-usa', 'us');
        setupClickListener(
            'changecountry-usa-and-uot', ['us', 'pr', 'vi', 'gu', 'mp']);
      }


     
 



$('#search').click(function() {


 if($('#pac-input').val()!='')
       {
        
       var lat=$('#lat').val();
       var long=$('#long').val(); 

        var place=  $('#place').val();
         var addr=  $('#address').val();
         var q2=$('#pac-input').val();
 

                        var redirectUrl="<?php echo base_url(); ?>index.php/search/locations"
                        var form = $('<form action="' + redirectUrl + '" method="post">' +
                            '<input type="hidden" name="place" value="'+ place +'" />' +
                            '<input type="hidden" name="addr" value="'+ addr +'" />' +
                            '<input type="hidden" name="lat" value="'+ lat +'" />' +
                            '<input type="hidden" name="q2" value="'+ q2 +'" />' +
                            '<input type="hidden" name="long" value="'+ long +'" />' +
                             
                           '</form>');
                            $('body').append(form);
                            $(form).submit();
}
else
{
  alert("Search Box Cannot Be Empty !");
}

});


$('#pac-input').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
     //$('#search').click();

      
    
  }


  
});  


 localStorage.clear();


 





</script>

<script>
 
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
       console.log("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
   console.log("Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude );

    $('#lat').val(position.coords.latitude);
    $('#long').val(position.coords.longitude); 

          $('#place').val("Location");
          $('#address').val("What's around you ?");

            var lat=$('#lat').val();
       var long=$('#long').val(); 

        var place=  $('#place').val();
         var addr=  $('#address').val();
         var q2=$('#pac-input').val();
 

                        var redirectUrl="<?php echo base_url(); ?>index.php/search/locations"
                        var form = $('<form action="' + redirectUrl + '" method="post">' +
                            '<input type="hidden" name="place" value="'+ place +'" />' +
                            '<input type="hidden" name="addr" value="'+ addr +'" />' +
                            '<input type="hidden" name="lat" value="'+ lat +'" />' +
                            '<input type="hidden" name="q2" value="'+ q2 +'" />' +
                            '<input type="hidden" name="long" value="'+ long +'" />' +
                             
                           '</form>');
                            $('body').append(form);
                            $(form).submit();




}
</script>


  <script 
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkkzyW9zJvzYJPe-5vrK_Ggko-Ha4NDzM&libraries=places&callback=initMap" async defer></script>

   <script type="text/javascript">
       $(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})

</script>

  
  


    <!-- Bootstrap core JavaScript -->
 
  </body>

  <style type="text/css">
    *::-webkit-input-placeholder {
    color: white;
}
*:-moz-placeholder {
    /* FF 4-18 */
    color: white;
    opacity: 1;
}
*::-moz-placeholder {
    /* FF 19+ */
    color: white;
    opacity: 1;
}
*:-ms-input-placeholder {
    /* IE 10+ */
    color: white;
}
*::-ms-input-placeholder {
    /* Microsoft Edge */
    color: white;
}
*::placeholder {
    /* modern browser */
    color: white;
}


.btn_gradient{

cursor: pointer;
transition: .5s ease;
color:white;
background: linear-gradient(40deg,#45cafc,#303f9f)!important;
border-radius: 10em;
box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
margin: .375rem;
border: 0;
text-transform: uppercase;
white-space: normal;
word-wrap: break-word;
font-weight: 400;
display: inline-block;
text-align: center;

font-family: Lato,Helvetica,Arial,sans-serif;
letter-spacing: 2px;
user-select: none;
align-items: flex-start;
padding: 13px;

  
}
  </style>




</html>
