<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("module/header_client" );
?>
		
	<div class="gtco-loader"></div>
	
	<div id="page">
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 text-right gtco-contact">
					<ul class="">
						<li><a href="#"><i class="ti-mobile"></i> +1 (0)123 456 7890 </a></li>
						<li><a href="http://twitter.com/gettemplatesco"><i class="ti-twitter-alt"></i> </a></li>
						<li><a href="#"><i class="icon-mail2"></i></a></li>
						<li><a href="#"><i class="ti-facebook"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.html">IT  BD<em>  71 LTD. </em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
						<li class="active"><a href="index.html">Home</a></li>
						<li><a href="about.html">About</a></li>
						<li class="has-dropdown">
							<a href="services.html">Services</a>
							<ul class="dropdown">
								<li><a href="#">Web Design</a></li>
								<li><a href="#">eCommerce</a></li>
								<li><a href="#">Branding</a></li>
								<li><a href="#">API</a></li>
							</ul>
						</li>
						<li class="has-dropdown">
							<a href="#">Dropdown</a>
							<ul class="dropdown">
								<li><a href="#">HTML5</a></li>
								<li><a href="#">CSS3</a></li>
								<li><a href="#">Sass</a></li>
								<li><a href="#">jQuery</a></li>
							</ul>
						</li>
						<li><a href="portfolio.html">Portfolio</a></li>
						<li><a href="contact.html">Contact</a></li>
						<li><a class="btn btn-white btn-lg btn-outline" href="<?php echo site_url('admin/login'); ?>" ">Franchisee Login</a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<header id="gtco-header" class="gtco-cover" role="banner" style="background-image:url(images/img_bg_1.jpg);">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="display-t">
						<div class="display-tc">
							<h1 class="animate-box" data-animate-effect="fadeInUp">Your Business To The Next Level</h1>
							<h2 class="animate-box" data-animate-effect="fadeInUp">Lets Make Something Greate <em>Together</em> <a href="http://gettemplates.co/" target="_blank">Big</a></h2>
							<p class="animate-box" data-animate-effect="fadeInUp"><a href="#" class="btn btn-white btn-lg btn-outline">Get In Touch</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="gtco-features-3">
		<div class="gtco-container">
			<div class="gtco-flex">
				<div class="feature feature-1 animate-box" data-animate-effect="fadeInUp">
					<div class="feature-inner">
						<span class="icon">
							<i class="ti-search"></i>
						</span>
						<h3>Software & Web Development</h3>
						<p>We provide complete cutting edge solutions for any kind of web applications in various technologies such as Java, Asp.net and PHP . </p>
						<p><a href="#" class="btn btn-white btn-outline">Learn More</a></p>
					</div>
				</div>
				<div class="feature feature-2 animate-box" data-animate-effect="fadeInUp">
					<div class="feature-inner">
						<span class="icon">
							<i class="ti-announcement"></i>
						</span>
						<h3>Mobile Application Development</h3>
						<p>For IOS, Android, PhoneGAP ,Over 6 million people use different mobile applications for their daily needs. We create content based on local needs and help different industry enterprises to go mobile. </p>
						<p><a href="#" class="btn btn-white btn-outline">Learn More</a></p>
					</div>
				</div>
				<div class="feature feature-3 animate-box" data-animate-effect="fadeInUp">
					<div class="feature-inner">
						<span class="icon">
							<i class="ti-timer"></i>
						</span>
						<h3>ERP & CRM Solutions</h3>
						<p>We provide full suite of business software, including the modules are Accounting, CRM, Human Resources, Invoicing, Point of Sale, Project Management, Purchase, Warehouse Management & more. </p>
						<p><a href="#" class="btn btn-white btn-outline">Learn More</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="gtco-features">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Who  Are We ?</h2>
					<p>IT BD71 is an intelligent business solution & service provider established by highly experienced, skilled and certified IT Professional to offer Business Application Software Development, Web Development and other it solutions worldwide..</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="ti-vector"></i>
						</span>
						<h3>Pixel Perfect</h3>
					<p>We provide complete cutting edge solutions for any kind of web applications in various technologies such as Java, Asp.net and PHP .</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="ti-tablet"></i>
						</span>
						<h3>Fully Responsive</h3>
						<p>We provide complete cutting edge solutions for any kind of web applications in various technologies such as Java, Asp.net and PHP .</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="ti-settings"></i>
						</span>
						<h3>Web Development</h3>
						<p>We provide complete cutting edge solutions for any kind of web applications in various technologies such as Java, Asp.net and PHP .</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="ti-ruler-pencil"></i>
						</span>
						<h3>Web Design</h3>
						<p>We provide complete cutting edge solutions for any kind of web applications in various technologies such as Java, Asp.net and PHP .</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="gtco-portfolio" class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Our Latest Works</h2>
					<p>We provide complete cutting edge solutions for any kind of web applications in various technologies such as Java, Asp.net and PHP .</p>
				</div>
			</div>

			<div class="row row-pb-md">
				<div class="col-md-12">
					<ul id="gtco-portfolio-list">
						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/img_1.jpg); "> 
							<a href="#" class="color-1">
								<div class="case-studies-summary">
									<span>Web Design</span>
									<h2>View the Earth from the Outer Space</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/img_2.jpg); ">
							<a href="#" class="color-2">
								<div class="case-studies-summary">
									<span>Illustration</span>
									<h2>Sleeping in the Cold Blue Water</h2>
								</div>
							</a>
						</li>


						<li class="one-half animate-box" data-animate-effect="fadeIn" style="background-image: url(images/img_3.jpg); ">
							<a href="#" class="color-3">
								<div class="case-studies-summary">
									<span>Illustration</span>
									<h2>Building Builded by Man</h2>
								</div>
							</a>
						</li>
						<li class="one-half animate-box" data-animate-effect="fadeIn" style="background-image: url(images/img_4.jpg); ">
							<a href="#" class="color-4">
								<div class="case-studies-summary">
									<span>Web Design</span>
									<h2>The Peaceful Place On Earth</h2>
								</div>
							</a>
						</li>

						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/img_5.jpg); "> 
							<a href="#" class="color-5">
								<div class="case-studies-summary">
									<span>Web Design</span>
									<h2>I'm Getting Married</h2>
								</div>
							</a>
						</li>
						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/img_6.jpg); ">
							<a href="#" class="color-6">
								<div class="case-studies-summary">
									<span>Illustration</span>
									<h2>Beautiful Flowers In The Air</h2>
								</div>
							</a>
						</li>
					</ul>		
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-md-offset-4 text-center animate-box">
					<a href="#" class="btn btn-white btn-outline btn-lg btn-block">See All Our Works</a>
				</div>
			</div>

			
		</div>
	</div>

	<div id="gtco-counter" class="gtco-section">
		<div class="gtco-container">

			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Fun Facts</h2>
					<p>Applications Development / OpenERP & CRM / Web Development / Software Development, Support & Consultancy / Customer Solutions / CCTV Camera / Access Control Solution</p>
				</div>
			</div>

			<div class="row">
				
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-settings"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="22070" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Creativity Fuel</span>

					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-face-smile"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="97" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Happy Clients</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-briefcase"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="402" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Projects Done</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-time"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="212023" data-speed="5000" data-refresh-interval="50">1</span>
						<span class="counter-label">Hours Spent</span>

					</div>
				</div>
					
			</div>
		</div>
	</div>

	<div id="gtco-products">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Products</h2>
					<p>Applications Development / OpenERP & CRM / Web Development / Software Development, Support & Consultancy / Customer Solutions / CCTV Camera / Access Control Solution</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="owl-carousel owl-carousel-carousel">
					<div class="item">
						<img src="<?php echo base_url(); ?>/assets/images/img_1.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
					<div class="item">
						<img src="<?php echo base_url(); ?>/assets/images/img_2.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
					<div class="item">
						<img src="<?php echo base_url(); ?>/assets/images/img_3.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
					<div class="item">
						<img src="<?php echo base_url(); ?>/assets/images/img_4.jpg" alt="Free HTML5 Bootstrap Template by GetTemplates.co">
					</div>
				</div>
			</div>
		</div>
	</div>

	

	<div id="gtco-subscribe">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Subscribe</h2>
					<p>Be the first to know about the new templates.</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-12">
					<form class="form-inline">
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Your Email">
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label for="name" class="sr-only">Name</label>
								<input type="text" class="form-control" id="name" placeholder="Your Name">
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<button type="submit" class="btn btn-default btn-block">Subscribe</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


<?php
 
$this->load->view("module/footer_client" );
?>
	
