<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='work';
$this->load->view("module/fr_header",$dat);
?>
<script src="<?php echo base_url(); ?>/assets/cal/moment.min.js"></script>
   <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/cal/bootstrap-datetimepicker.css">

<!-- jQuery library -->
<script src="<?php echo base_url(); ?>/assets/cal/bootstrap-datetimepicker.min.js"></script>

   
<div class="container" style="width:50%;text-align: center;">
    <div class="card card-container" style="padding:5%">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" style="width: 120px;margin:0 auto" src="<?php echo base_url(); ?>assets/images/form.png" />
        <br/>
        <h2 id="profile-name" class="profile-name-card">Work Form</h2>
        <br/>

  <?php $attributes = array("class" => "", "id" => "registerForm", "name" => "registerForm");
          echo form_open("franchisee/insert_work", $attributes);?>

            <div class="form-group">
                                                    <input type="text" placeholder="First Name" required id="fname" name="fname" class="form-control" value="<?php echo set_value('fname'); ?>">

                                                    <span class="text-danger"><?php echo form_error('fname'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="text" placeholder="Last Name" required id="lname" name="lname" class="form-control" value="<?php echo set_value('lname'); ?>">

                                                      <span class="text-danger"><?php echo form_error('lname'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="text" placeholder="Record No" required id="mobile" name="rec" class="form-control" 
                                                    value="<?php echo set_value('rec'); ?>">

                                                      <span class="text-danger"><?php echo form_error('rec'); ?></span>
                                                </div>
                                                
                                                  <div class="form-group">
                                                    <input type="text" placeholder="Policy Date" required id="policyDate" name="policyDate"  class="form-control" 
                                                    value="<?php echo set_value('policyDate'); ?>">

                                                      <span class="text-danger"><?php echo form_error('policyDate'); ?></span>
                                                </div>
                                                

                                                <div class="form-group">
                                                    <input type="text" placeholder="Policy No" required id="policy" name="policy"  class="form-control" 
                                                    value="<?php echo set_value('policy'); ?>">

                                                      <span class="text-danger"><?php echo form_error('policy'); ?></span>
                                                </div>


                                              <div class="form-group">
                                                    <input type="text" placeholder="city" required id="city" name="city"  class="form-control" 
                                                    value="<?php echo set_value('city'); ?>">

                                                      <span class="text-danger"><?php echo form_error('city'); ?></span>
                                                </div>
                                                
                                                  <div class="form-group">
                                                    <input type="text" placeholder="state" required id="state" name="state"  class="form-control" 
                                                    value="<?php echo set_value('state'); ?>">

                                                      <span class="text-danger"><?php echo form_error('state'); ?></span>
                                                </div>
                                                
                                                 <div class="form-group">
                                                    <input type="number" required placeholder="Mobile No." id="mobile" name="mobile" class="form-control" value="<?php echo set_value('mobile'); ?>">

                                                      <span class="text-danger"><?php echo form_error('mobile'); ?></span>
                                                </div>
                                                
                                                

                                                 <div class="form-group">
                                                  <select class="form-control" name="marital">
                                                    <option value="Married">Married</option>
                                                    <option value="Un-married">Un-married</option>
                                                  </select>
                                                </div>
                                                
                                                 <div class="form-group">
                                                    <input type="text" placeholder="GP Code" required id="gp" name="gp" class="form-control" 
                                                    value="<?php echo set_value('gp'); ?>">

                                                      <span class="text-danger"><?php echo form_error('gp'); ?></span>
                                                </div>
                                            
                                            
                                            
                                               <div class="form-group">
                                                    <input type="text" placeholder="Claim Days" required id="claim" name="claim" class="form-control" 
                                                    value="<?php echo set_value('claim'); ?>">

                                                      <span class="text-danger"><?php echo form_error('claim'); ?></span>
                                                </div>

												<div class="form-group">
                                                    <input type="number" placeholder="Paid Amount" required id="paid" name="paid" class="form-control" 
                                                    value="<?php echo set_value('paid'); ?>">

                                                      <span class="text-danger"><?php echo form_error('paid'); ?></span>
                                                </div>
												
												
												<div class="form-group">
                                                    <input type="number" placeholder="Net Amount" required id="net" name="net" class="form-control" 
                                                    value="<?php echo set_value('net'); ?>">

                                                      <span class="text-danger"><?php echo form_error('paid'); ?></span>
                                                </div>

            <button class="btn btn-primary btn-block btn-signin" type="submit"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Work</button>
           
             <?php echo form_close(); ?>
                             
                             <br/>
                             <?php echo $this->session->flashdata('msg'); ?>               
    </div><!-- /card-container -->
</div><!-- /container -->






<br/>
  <script type="text/javascript">
 
$('#policyDate').datetimepicker({
    format: "MM/DD/YYYY"


});

 
</script>
 
 </div>
  </body>
</html>






 