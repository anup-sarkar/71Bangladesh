<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='pro';
$this->load->view("module/fr_header",$dat);
?>

<div class="row">

	<div class="col-md-4">

		<div class="card text-center">
			<img src="<?php echo base_url(); ?>assets/images/user.png"
				class="card-img-top" alt="..." style="width: 150px; margin: 0 auto">
			<div class="card-body">
				<h5 class="card-title"><?php echo $User[0]['Fname']." ".$User[0]['Lname']; ?></h5>
				<h6 class="card-text">
					<i class="fa fa-envelope"></i> <?php echo $User[0]['Email']; ?></h6>
			</div>

			<div class="card-body">
				<a href="<?php echo site_url('Franchisee/profile_update'); ?>"
					class="btn btn-primary"><i class="fa fa-edit"></i> Update Profile</a>
				<a href="<?php echo site_url('Franchisee/tasks'); ?>"
					class="btn btn-primary"><i class="fa fa-download"></i> Work Form</a>
			</div>


			<div class="list-group" style="text-align: left">

				<a href="#" class="list-group-item list-group-item-action active"><i
					class="fa fa-phone"> &nbsp; <?php echo $User[0]['Mobile']; ?></i> </a>
				<a href="#" class="list-group-item list-group-item-action">User Gender&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :  <?php echo $User[0]['Sex']==null ? "Not Set" : $User[0]['Sex'] ; ?></a>
				<a href="#" class="list-group-item list-group-item-action">Date Of Birth &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:  <?php echo $User[0]['DOB']==null ? "Not Set" : $User[0]['DOB'] ; ?></a>
				<a href="#" class="list-group-item list-group-item-action">National ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:  <?php echo $User[0]['NID']==null ? "Not Set" : $User[0]['NID'] ;?></a>

				<a href="#" class="list-group-item list-group-item-action">SIS Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :  <?php echo $User[0]['SIS']==null ? "Not Set" : $User[0]['SIS'] ;?></a>

				<a href="#" class="list-group-item list-group-item-action">Slot Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; :  <?php echo $User[0]['Slot']==null ? "Not Set" : $User[0]['Slot'] ;?></a>

				<a href="#" class="list-group-item list-group-item-action">ID Creation Date&nbsp;&nbsp;:  <?php echo $User[0]['IDCreationDate']==null ? "Not Set" : $User[0]['IDCreationDate'] ;?></a>



			</div>

		</div>
	</div>


	<div class="col-md-8">



		<div class="card text-center">
			<div class="card-header">Assigned Files</div>
			<div class="card-body">



				<div class="row" style="margin-left: 6px">   
<?php

$i = 1;
foreach ($Files as $item) {
    $url = base_url();
    $path = $item->Path;
    $name = $item->Name;
    $details = $item->Details;

    ?>
        
<div class="card bg-light   mr-4" style="">
						<div class="card-header">File  (<?php echo $name;?>)</div>
						<div class="card-body">
		<?php

echo "  <a class='btn btn-success' href='" . $url . "index.php/franchisee/download?path=" . $path . "&name=" . $name . "' ><i class='fa fa-download' aria-hidden='true'></i> Downloads " . $i . "</a>   ";
    ?>

		<h5 class="card-title" style="margin-top: 20px"><?php echo $details;?></h5>

						</div>
					</div>

<?php $i++; }?>
</div>
			</div>
			<div class="card-footer text-muted">You Can Download From Here.</div>
		</div>
		
		<?php echo $this->session->flashdata('msg'); ?>    
	</div>

</div>


<style>
.list-group-item {
	font-weight: bold;
}
</style>

</body>

</html>