<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='file';
$this->load->view("module/admin_header",$dat );
?>

<div class="container" style="width: 50%; text-align: center;">
	<div class="card card-container" style="padding: 5%">
		<!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
		<img id="profile-img" class="profile-img-card"
			style="width: 200px; margin: 0 auto"
			src="<?php echo base_url(); ?>assets/images/up.png" />
		<h3 id="profile-name" class="profile-name-card">Document Upload</h3>
		<br />

  <?php

$attributes = array(
    "class" => "",
    "id" => "registerForm",
    "name" => "registerForm"
);
echo form_open_multipart("admin/upload", $attributes);
?>

            <div class="form-group">
			<input type="text" placeholder="File Name" id="name" name="name" required
				class="form-control" value="<?php echo set_value('fname'); ?>"> <span
				class="text-danger"><?php echo form_error('name'); ?></span>
		</div>






		<div class="form-group">
			<input type="text" placeholder="Enter Details" id="details" name="details"
				class="form-control" value="<?php echo set_value('details'); ?>" />
				 <span
				class="text-danger"><?php echo form_error('details'); ?>
				</span>
		</div>


		 

		<div class="form-group">
			<select class="form-control" name="uid">
				<option value="0" selected>Targeted To All </option>
			<?php  
			foreach ($Users as $item)
            {
                   
                  $id=$item->id; 
                  $name=$item->Fname." ".$item->Lname; 
              
                  
                  
          	 ?>
				<option value="<?php  echo $id; ?>"><?php echo $name; ?></option>
				 
				
				<?php  } ?>
			
			</select>
		</div>
		
		<div class="form-group">
			<input type="file" name="userfile" class="form-control" />
		</div>




		<button class="btn btn-primary btn-block btn-signin" type="submit">
			<i class="fa fa-file" aria-hidden="true"></i> Upload
		</button>
           
             <?php echo form_close(); ?>
                             
                             <br />
                             <?php echo $this->session->flashdata('msg'); ?>               
    </div>
	<!-- /card-container -->
</div>
<!-- /container -->






<br />


</div>
</body>
</html>






