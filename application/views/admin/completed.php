<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$dat['flag']='work';
$this->load->view("module/admin_header",$dat );
?>




      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="padding: 20px">
                            <div class="header">
                            
                            
                                <h2 class="title"><i class="fa fa-file-text" aria-hidden="true"></i> Completed  Work Forms  
                                
                                
                                <a class="btn btn-primary" id="addBtn"
                                href="<?php echo base_url().'index.php/franchisee/work_form' ?>" 
                                style="float: right">
                                
                                <i class="fa fa-plus-circle"></i> Add Work Forms </a> 
                                
                                
                                </h2>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover" style="font-size:12px" >
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Name</th>
                                  
                                       <th>Mobile</th>
                                      <th>City</th>
                                      
                                        <th>State</th>
                                  
                                    
                                    
                                  
                                   <th>Record No.</th>
                                    <th>Policy Date</th>
                                         <th>Policy No</th>
                                     <th>GP Code</th>
                                      <th>Claim Days</th>
                                      <th>Paid Amt.</th>
                                       <th>Net Amt.</th>
                                       <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($Forms as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->Fname." ".$item->Lname; 
                  $text="";
              



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      
                                       echo "<td>".$name."</td>";   
                                  
                                          echo "<td>".$item->Mobile."</td>";
                                           echo "<td>".$item->City."</td>";
                                           echo "<td>".$item->State."</td>";
                                           echo "<td>".$item->RecordNo."</td>";
                                           echo "<td>".$item->PolicyDate."</td>";
                                           echo "<td>".$item->PolicyNo."</td>";
                                           echo "<td>".$item->GpCode."</td>";
                                           echo "<td>".$item->ClaimDays."</td>";
                                           echo "<td>".$item->NetAmount."</td>";
                                           echo "<td>".$item->PaidAmount."</td>";
                                    
                                  

                                  
                                

                                       echo "<td> <a class='btn btn-danger' id='dltBtn' href='".$url."index.php/franchisee/delete_work/".$id."' ><i class='fa fa-times' aria-hidden='true'></i>   </a>  </td>
                                      ";
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>

  
 <style>
<!--

-->

<?php   if($_SESSION['type']=="Super User") {?>

 #dltBtn
{
    display:none;
    
 }
 
 #addBtn 
{
    display:none;
    
 }
 
 <?php } ?>
</style>

<br/>
 
 
 </div>
  </body>
</html>

