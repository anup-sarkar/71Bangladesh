<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='file';
$this->load->view("module/admin_header",$dat );
?>


      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="padding: 20px">
                            <div class="header">
                                <h2 class="title">All Files <a class="btn btn-primary" 
                                href="<?php echo base_url().'index.php/admin/file_upload' ?>" 
                                style="float: right">Upload New </a> </h2>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover"  >
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>File Name</th>
                                  
                                       <th>Details</th>
                                      <th>Assigned To</th>
                                  
                                      <th>Download</th>
                                 
                                  
                                      <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($Files as $item)
            {
                $url=base_url();
                  $path=$item->Path; 
                  $name=$item->Name ; 
                  $details=$item->Details ;
                  
                  $FullName="";
                  
                  if($item->UserID == 0)
                  {
                      $FullName="For All !";
                  }
                  else {
                      $FullName=$item->Fname." ".$item->Lname;
                  }
               
               



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      
                                       echo "<td>".$name."</td>";   
                                  
                                          echo "<td>".$details."</td>";
                                          echo "<td><i class='fa fa-user-circle'></i> <b>  ".$FullName."</b></td>";
                                         
                                           echo "<td>  <a class='btn btn-success' href='".$url."index.php/franchisee/download?path=".$path."&name=".$name."' ><i class='fa fa-download' aria-hidden='true'></i> Download</a> </td>";
                                           
                                           
                                           
                                         echo "<td> <a class='btn btn-danger' href='".$url."index.php/admin/file_delete/?id=".$item->id."&path=".$item->FileName."' ><i class='fa fa-times' aria-hidden='true'></i></a>  </td>";

                                  
                                

                                      
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>

  


<br/>
 
 
 </div>
  </body>
</html>

