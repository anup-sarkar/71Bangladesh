<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Forms extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'UserID' => array(
                                'type' => 'INT',
                                'constraint' => 5
          
                        ),
                        'Fname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'Lname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'RecordNo' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                        'PolicyDate' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '10',
                        ),

                        'PolicyNo' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        )
                        ,
                        'City' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),

                        'State' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),


                        'Mobile' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                        ),
                        'MaritalStatus' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                         'GpCode' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '30',
                        ),
                         'ClaimDays' => array(
                           'type' => 'INT',
                                'constraint' => 5,
                        ),
                         'PaidAmount' => array(
                           'type' => 'INT',
                                'constraint' => 5,
                        ),
                         'NetAmount' => array(
                           'type' => 'INT',
                                'constraint' => 5,
                        )
                        ,'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        , 'uppdated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                 
                $this->dbforge->create_table('Forms');

        }

        public function down()
        {
                $this->dbforge->drop_table('Forms');
        }
}